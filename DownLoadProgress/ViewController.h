//
//  ViewController.h
//  DownLoadProgress
//
//  Created by Sandip Saha on 13/02/14.
//  Copyright (c) 2014 Sandip Saha. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *imageView;            //ImageView will cointain the downloaded image
@property (weak, nonatomic) IBOutlet UILabel *progressLabel;            //Label showing the progress percentage
@property (weak, nonatomic) IBOutlet UITextView *imageURLTextView;      //Label will show the URL of the image currently downloaded
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;  //Activity indicator while download in pogress
@property (weak, nonatomic) IBOutlet UIProgressView *progressView;      //Progress bar for showing the the download progression
@property (weak, nonatomic) IBOutlet UIStepper *stepper;                //Stepper to download different images from imageURL array

@property BOOL downloadInProgress;                                      //This boolean showing whether download is in progress or not

- (IBAction)stepperAction:(id)sender;
@end
