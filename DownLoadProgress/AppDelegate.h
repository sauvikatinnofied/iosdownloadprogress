//
//  AppDelegate.h
//  DownLoadProgress
//
//  Created by Sandip Saha on 13/02/14.
//  Copyright (c) 2014 Sandip Saha. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
