//
//  ViewController.m
//  DownLoadProgress
//
//  Created by Sandip Saha on 13/02/14.
//  Copyright (c) 2014 Sandip Saha. All rights reserved.
//

#import "ViewController.h"
#import "NetworkResourceDownLoader.h"


//private elements
@interface ViewController ()
{
    NSArray *imageURLs;
    UIAlertView *downloadInProgressAlert;
}
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    
    
    
    
    //Initializing the imageURLs with http://www.flickr.com/ images
    imageURLs = @[
                  @"http://farm9.staticflickr.com/8309/7982821531_92dd8e42f4_b.jpg",
                  @"http://farm9.staticflickr.com/8171/8027606971_7bc7edb437_b.jpg",
                  @"http://farm9.staticflickr.com/8435/8007856466_cce6ff49f9_b.jpg",
                  @"https://farm8.staticflickr.com/7456/9384088167_53ddfe91f3_b.jpg",
                  @"https://farm4.staticflickr.com/3668/11646984085_a66d45929a_b.jpg",
                  @"https://farm7.staticflickr.com/6221/6276888492_f3cfa5a51d_b.jpg",
                  @"https://farm4.staticflickr.com/3786/9667083662_2bcac5aec3_b.jpg",
                  @"http://farm6.staticflickr.com/5290/5688323310_cb8057501b_b.jpg",
                  @"http://farm3.staticflickr.com/2866/12920801553_8e210329c4_b.jpg",
                  @"http://farm3.staticflickr.com/2886/12858797154_26c0f867dd_o.jpg",
                  @"http://farm8.staticflickr.com/7338/12918964355_77e8407ab7_o.jpg"
                 ];
    
    
    //Setting stepper's minimum,maximum and current value
    _stepper.minimumValue = 0;
    _stepper.maximumValue =imageURLs.count-1;
    _stepper.value = 0;
    
    _downloadInProgress = FALSE;
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)stepperAction:(UIStepper*)sender {
    
    if (!_downloadInProgress) {
        
        _downloadInProgress = TRUE;
        [_spinner startAnimating];
        
        //Showing the image URL which is going to be downloaded
        _imageURLTextView.text = [imageURLs objectAtIndex:sender.value];
        
        
        NetworkResourceDownLoader *imageDownloader = [[NetworkResourceDownLoader alloc]initWithBaseURL:[imageURLs objectAtIndex:sender.value]
                                                                                           obejectPath:@""];
        
        __weak NetworkResourceDownLoader *weakImageDownloader = imageDownloader; //Taking weak reference to avoid retain cycle
        
        //Defining  completion handler block
        [weakImageDownloader setCompletionHandler:^{
            
             _downloadInProgress = FALSE;
            
            //Initializing the imageView's image with the downloaded data
            _imageView.image = [UIImage imageWithData:weakImageDownloader.responseData];
            
            //stopping the activity indicator
            [_spinner stopAnimating];
           
            //Hiding the downloadInProgressAlert if any
            if (downloadInProgressAlert) {
                [downloadInProgressAlert dismissWithClickedButtonIndex:0 animated:YES];
            }
            
        }];
        
        //Defining progress reporter block
        [weakImageDownloader setProgressReporter:^{
            
            _progressLabel.text = [NSString stringWithFormat:@"%0.2f%@",weakImageDownloader.downloadProgressFraction*100,@"%"];
            _progressView.progress = weakImageDownloader.downloadProgressFraction;
            
        }];
        
        //Defining the error handler block
        [weakImageDownloader setErrorHandler:^(NSError *error) {
            
            _downloadInProgress = FALSE;
            NSString *errorString = [NSString stringWithFormat:@"%@.Connection Status Code = %d",error.localizedDescription,
                                     weakImageDownloader.connectionStatusCode];
            UIAlertView *downloadAlert = [[UIAlertView alloc]initWithTitle:@"Download Error" message:errorString delegate:self
                                                         cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [downloadAlert show];
            
            
            
        }];
        
        //Starting the download
        [weakImageDownloader startDownload];
    }
    else//Download is in progress
    {
        //Showing an alert 
        downloadInProgressAlert = [[UIAlertView alloc]initWithTitle:@"Sorry" message:@"Download in progress,please wait until it completes" delegate:self
                                                     cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [downloadInProgressAlert show];
    }
    
}
@end
