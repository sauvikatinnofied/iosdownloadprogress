//
//  NetworkResourceDownLoader.m
//  DownLoadProgress
//
//  Created by Sandip Saha on 13/02/14.
//  Copyright (c) 2014 Sandip Saha. All rights reserved.
//

#import "NetworkResourceDownLoader.h"


//Declaring the private variables
@interface NetworkResourceDownLoader() <NSURLConnectionDataDelegate>
{
    NSURL *url;
    NSURLConnection *urlConnection;
    NSString *responseString;
    int long long totalBytesToBeDownloaded;
}

@end

@implementation NetworkResourceDownLoader

-(id)initWithBaseURL:(NSString*)baseURL obejectPath:(NSString*)objectPath
{
    if(self = [super init]){
        
        if (!baseURL && !objectPath)//If both parameters are missing
        {
            return nil;
        }
        else{
            url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",baseURL,objectPath]];
        }
    }
    
    return  self;
}

-(void)startDownload
{
    if (url) {
        //creating the url connection
        urlConnection =[NSURLConnection connectionWithRequest:[NSURLRequest requestWithURL:url cachePolicy:NSURLCacheStorageNotAllowed timeoutInterval:60.0f]
                                                     delegate:self];
        }
}

-(void)cancelDownload
{
    //releasing the resources
    urlConnection = nil;
    _responseData = nil;
    _completionHandler = nil;
    _progressReporter = nil;
    _errorHandler = nil;
}





#pragma mark- NSURLConnectionDataDelagate Methods

//called only once when the NSURLConnection receives response from the server
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    //allocating memory for the data to be downloaded
    _responseData = [[NSMutableData alloc]init];
    totalBytesToBeDownloaded =0;
    
    //getting information about the content leangth
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
    totalBytesToBeDownloaded = httpResponse.expectedContentLength;
    _connectionStatusCode = httpResponse.statusCode;
    
    if (totalBytesToBeDownloaded < 0) {
        NSLog(@"NetworkRequestDownloader ERROR: Content length is not availavle from server ,download progress report is not possible.");
    }
    
}

//called when data chunk is received from the server
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    //appending the receied data
    [_responseData appendData:data];
    _downloadProgressFraction = (_responseData.length*1.0)/totalBytesToBeDownloaded;
    
    //if progressReporter block is defined ,going to execute that
    if(_progressReporter){
        _progressReporter();
    }
}

//called when netwrok connection failed
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"NetworkRequestDownloader ERROR:Download failed.");
    
    //collecting the error string
    _errorString = [error localizedDescription];
    
    
    //if error handler is defined then going to execute the error handler
    if (_errorHandler) {
        _errorHandler(error);
    }
    
}
-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSLog(@"NetworkRequestDownloader REPORTS:Download completed successfully.");
    
    //if completion handler is defined ,going to execute the completion handler
    if (_completionHandler) {
        _completionHandler();
    }
    
}

@end
